GraphQL server written in java, based on Spark java as backend server.
Makes use of [graphql-java](https://github.com/graphql-java/graphql-java) . 
Also [GraphQL SPQR](https://github.com/leangen/graphql-spqr) is used for schema generation (code based schema).

`-parameters` should be supplied als java compiler argument for spqr to work correctly (included in maven compiler configuration)


Run bij running main in GraphQLServer class

-parameters should be supplied als java compiler argument for spqr to work correctly

then go to [http://localhost:4567](http://localhost:4567)   (graphiql in-browser IDE)

Uses database in mongo-db. A docker container with mongo-db can be started by startMongo.sh

Also included a simple client for searching Persons 

This codebase is based on the exellent graphql java tutorial by [Bojan Tomic] (https://www.howtographql.com/graphql-java/0-introduction/) , replacing Jetty by Spark java. 

Example queries : 

create a Person:
```
mutation createPerson { 
	createPerson( newPerson: {name:"Pete", sex:Male }) {
  	personId
  	name
  	sex
  }
}
```

Find all persons:
``` 
query allPersons {
	findPersons {
    personId
    name}
}  
```

Find Person with name "Maria":
``` 
query findMaria {
  findPersons(filter:{name_contains:"Maria"}) {
    name
    sex
  }
}

```

get Person based on id:
``` 
query getPerson {
  getPersonById(id:"5a226fc27e621102fc4e47a2") {
    name,
    sex
  }
}


```