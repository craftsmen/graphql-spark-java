package nl.gerlo.graphql;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class GraphQLRequest {

    private ObjectMapper mapper = new ObjectMapper();

    public GraphQLRequest() {
    }

    public GraphQLRequest(String query) {
        this.query = query;
    }

    private String query;


    public String getQuery() {
        return query;
    }

    public String toJson() throws JsonProcessingException {
        return mapper.writeValueAsString(this);
    }
}
