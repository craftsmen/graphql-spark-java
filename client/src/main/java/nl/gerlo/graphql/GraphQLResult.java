package nl.gerlo.graphql;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public abstract class GraphQLResult<T> {

    private static ObjectMapper mapper = new ObjectMapper();

    public GraphQLResult() {
    }


    private Map<String,T> data;

    private List<Object> errors;

    public T getQueryResult(String query) {
        if (errors!=null && !errors.isEmpty()){
            System.out.println(errors);
        }
        return data.get(query);
    }

    public Map<String, T> getData() {
        return data;
    }


    public Object getErrors() {
        return errors;
    }
}
