package nl.gerlo.graphql.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import nl.gerlo.graphql.GraphQLResult;

import java.io.IOException;
import java.util.List;

public class MultiplePersonsResult extends GraphQLResult<List<Person>> {

    private static ObjectMapper mapper = new ObjectMapper();


    public static List<Person> getResult(String json, String query) throws IOException {
        return  mapper.readValue(json,MultiplePersonsResult.class).getQueryResult(query);
    }
}
