package nl.gerlo.graphql.api;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.stream.Collectors;

public class Person {


    public enum Sex {
        Male ,
        Female
    }

    private String personId;
    private String name;
    private Sex sex;


    public Person() {
    }


    public Person(String personId, String name, Sex sex) {
        this.personId = personId;
        this.name = name;
        this.sex = sex;
    }

    public String getName() {
        return name;
    }

    public String getPersonId() {
        return personId;
    }

    public Sex getSex() {
        return sex;
    }

    @Override
    public String toString() {
        return "Person{" +
                "personId='" + personId + '\'' +
                ", name='" + name + '\'' +
                ", sex=" + sex +
                '}';
    }

    public static String getAllFieldNames() {
        return "{" + Arrays.stream(Person.class.getDeclaredFields())
                .map(Field::getName)
                .collect(Collectors.joining(",")) + "}";
    }
}
