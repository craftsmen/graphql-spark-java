package nl.gerlo.graphql.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import nl.gerlo.graphql.GraphQLResult;

import java.io.IOException;
import java.util.List;

public class SinglePersonResult extends GraphQLResult<Person> {

    private static ObjectMapper mapper = new ObjectMapper();


    public static Person getResult(String json, String query) throws IOException {
        return  mapper.readValue(json,SinglePersonResult.class).getQueryResult(query);
    }
}
