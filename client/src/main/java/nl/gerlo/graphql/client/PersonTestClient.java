package nl.gerlo.graphql.client;

import nl.gerlo.graphql.GraphQLRequest;
import nl.gerlo.graphql.api.MultiplePersonsResult;
import nl.gerlo.graphql.api.Person;
import nl.gerlo.graphql.api.SinglePersonResult;
import org.apache.http.client.fluent.Request;
import org.apache.http.entity.ContentType;

import java.io.IOException;
import java.util.List;

import static nl.gerlo.graphql.api.Person.Sex.Female;

public class PersonTestClient {

    private static GraphQLRequest createPerson(String name, Person.Sex sex) {
        return new GraphQLRequest(String.format("mutation {  createPerson( newPerson: {name:\"%s\", sex:%s }) " + Person.getAllFieldNames() + "}", name, sex.name()));
    }

    private static GraphQLRequest allPersons() {
        return new GraphQLRequest("query {findPersons " + Person.getAllFieldNames() + "}");
    }

    private static GraphQLRequest personQuery(String id) {
        return new GraphQLRequest(String.format("query  { getPersonById(id:\"%s\" ) " + Person.getAllFieldNames() + "}", id));
    }

    private static GraphQLRequest personQueryByName(String name) {
        return new GraphQLRequest(String.format("query  { findPersons(filter:{name_contains:\"%s\"} ) " + Person.getAllFieldNames() + "}", name));
    }

    public static void main(String[] args) throws IOException {

        String queryResponse = doGraphQLQuery(createPerson("Marie", Female));
        Person person = SinglePersonResult.getResult(queryResponse, "createPerson");
        System.out.println(person);

        queryResponse = doGraphQLQuery(allPersons());
        List<Person> persons = MultiplePersonsResult.getResult(queryResponse, "findPersons");
        System.out.println(persons);

        queryResponse = doGraphQLQuery(personQuery(persons.get(0).getPersonId()));
        person = SinglePersonResult.getResult(queryResponse, "getPersonById");
        System.out.println(person);

        queryResponse = doGraphQLQuery(personQueryByName("Marie"));
        persons = MultiplePersonsResult.getResult(queryResponse, "findPersons");
        System.out.println(persons);

    }


    private static String doGraphQLQuery(GraphQLRequest request) throws IOException {
        return Request.Post("http://localhost:4567/graphql")
                .bodyString(request.toJson(), ContentType.APPLICATION_JSON)
                .execute()
                .returnContent().asString();
    }
}
