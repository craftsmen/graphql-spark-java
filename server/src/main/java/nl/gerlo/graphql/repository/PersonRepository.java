package nl.gerlo.graphql.repository;

import com.mongodb.client.MongoCollection;
import nl.gerlo.graphql.schema.Person;
import nl.gerlo.graphql.schema.PersonFilter;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Filters.regex;

public class PersonRepository {

    private final MongoCollection<Document> persons;

    public PersonRepository(MongoCollection<Document> persons) {
        this.persons = persons;
    }

    public Person findById(String personId) {
        Document doc = persons.find(eq("_id", new ObjectId(personId))).first();
        return person(doc);
    }

    public List<Person> getPersons(PersonFilter filter) {
        Optional<Bson> mongoFilter = Optional.ofNullable(filter).map(this::buildFilter);

        List<Person> allPersons = new ArrayList<>();
        for (Document doc : mongoFilter.map(persons::find).orElseGet(persons::find)) {
            allPersons.add(person(doc));
        }
        return allPersons;
    }

    //builds a Bson from a PersonFilter
    private Bson buildFilter(PersonFilter filter) {
        String namePattern = filter.getNameContains();
        Bson nameCondition = null;
        if (namePattern != null && !namePattern.isEmpty()) {
            nameCondition = regex("name", ".*" + namePattern + ".*", "i");
        }
        return nameCondition;
    }

    public Person savePerson(Person person) {
        Document doc = new Document();
        doc.append("name", person.getName());
        doc.append("sex", person.getSex().name());
        persons.insertOne(doc);
        return person(doc);
    }

    private Person person(Document doc) {
        return new Person(
                doc.get("_id").toString(),
                doc.getString("name"),
                Person.Sex.valueOf(doc.getString("sex")));
    }
}