package nl.gerlo.graphql.schema;

import io.leangen.graphql.annotations.GraphQLMutation;
import nl.gerlo.graphql.repository.PersonRepository;

public class Mutation  {

    private final PersonRepository personRepository;

    public Mutation(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    @GraphQLMutation
    public Person createPerson(Person newPerson) {
        return personRepository.savePerson(newPerson);
    }
}