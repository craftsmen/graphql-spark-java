package nl.gerlo.graphql.schema;

public class Person {

    public enum Sex {
        Male ,
        Female
    }

    private String personId;
    private String name;
    private Sex sex;


    public Person() {
    }


    public Person(String personId, String name, Sex sex) {
        this.personId = personId;
        this.name = name;
        this.sex = sex;
    }

    public String getName() {
        return name;
    }

    public String getPersonId() {
        return personId;
    }

    public Sex getSex() {
        return sex;
    }
}