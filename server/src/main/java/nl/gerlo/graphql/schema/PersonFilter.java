package nl.gerlo.graphql.schema;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PersonFilter {

    private String nameContains;

    @JsonProperty("name_contains") //the field name in the graphql schema
    public String getNameContains() {
        return nameContains;
    }

    public void setNameContains(String nameContains) {
        this.nameContains = nameContains;
    }

}