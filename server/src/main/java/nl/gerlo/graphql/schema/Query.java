package nl.gerlo.graphql.schema;

import io.leangen.graphql.annotations.GraphQLQuery;
import nl.gerlo.graphql.repository.PersonRepository;

import java.util.List;

public class Query {

    private final PersonRepository personRepository;

    public Query(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    @GraphQLQuery
    public List<Person> findPersons(PersonFilter filter) {
        return personRepository.getPersons(filter);
    }

    @GraphQLQuery
    public Person getPersonById(String id) {
        return personRepository.findById(id);
    }
}