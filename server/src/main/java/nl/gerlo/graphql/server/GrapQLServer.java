package nl.gerlo.graphql.server;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoDatabase;
import graphql.ExecutionResult;
import graphql.GraphQL;
import graphql.GraphQLError;
import graphql.InvalidSyntaxError;
import graphql.schema.GraphQLSchema;
import graphql.validation.ValidationError;
import io.leangen.graphql.GraphQLSchemaGenerator;
import nl.gerlo.graphql.repository.PersonRepository;
import nl.gerlo.graphql.schema.Mutation;
import nl.gerlo.graphql.schema.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.Spark;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static spark.Spark.*;

public class GrapQLServer {

    private static Logger log = LoggerFactory.getLogger(GrapQLServer.class);


    private static ObjectMapper mapper = new ObjectMapper();


    public static void main(String[] args) {
        String ENV_PORT = System.getenv().get("PORT");
        port(ENV_PORT == null ? 4567 : Integer.parseInt(ENV_PORT));

        Spark.staticFileLocation("/static");

        MongoDatabase mongo = new MongoClient().getDatabase("person");

        //CORS
        before((request, response) -> response.header("Access-Control-Allow-Origin", "*"));

        GraphQLSchema schema = graphQLServerSetupSchema(mongo);

        post("/graphql", (request, response) -> {
            GraphQLRequest graphQLRequest = mapper.readValue(request.body(), GraphQLRequest.class);
            ExecutionResult executionResult = new GraphQL.Builder(schema)
                    .build()
                    .execute(graphQLRequest.getQuery(), graphQLRequest.getOperationName(), (Object) null);
            response.type("application/json");
            return mapper.writeValueAsString(createResultFromDataAndErrors(executionResult.getData(), executionResult.getErrors()));
        });

    }

    private static GraphQLSchema graphQLServerSetupSchema(MongoDatabase mongo) {
        PersonRepository personRepository = new PersonRepository(mongo.getCollection("persons"));
        Query query = new Query(personRepository);
        Mutation mutation = new Mutation(personRepository);
        return new GraphQLSchemaGenerator().withOperationsFromSingletons(query, mutation).generate();
    }


    private static Map<String, Object> createResultFromDataAndErrors(Object data, List<GraphQLError> errors) {

        final Map<String, Object> result = new HashMap<>();
        result.put("data", data);

        if (errorsPresent(errors)) {
            final List<GraphQLError> clientErrors = filterGraphQLErrors(errors);
            if (clientErrors.size() < errors.size()) {
                errors.stream()
                        .filter(error -> !isClientError(error))
                        .forEach(error -> log.error("Error executing query ({}): {}", error.getClass().getSimpleName(), error.getMessage()));
            }
            result.put("errors", clientErrors);
        }

        return result;
    }

    private static boolean errorsPresent(List<GraphQLError> errors) {
        return errors != null && !errors.isEmpty();
    }

    private static boolean isClientError(GraphQLError error) {
        return error instanceof InvalidSyntaxError || error instanceof ValidationError;
    }

    private static List<GraphQLError> filterGraphQLErrors(List<GraphQLError> errors) {
        return errors.stream()
                .filter(GrapQLServer::isClientError)
                .collect(Collectors.toList());
    }
}
